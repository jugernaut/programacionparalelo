package hilos.tiaAly.concurrente;

import hilos.tiaAly.secuencial.Cliente;

/**
 * Clase que simula las caracteristicas que nos interesan de un trabjador.
 * Mediante la clase Thread, los Trabajadores pueden funcionar de manera
 * concurrente
 * @author mike
 */
public class Trabajador implements Runnable {

    private String nombre;
    private Cliente cliente;
    private long initialTime;
    
    /**
     * Constructor de trabajadores
     * @param nombre Nombre del trabajador
     * @param cliente Cliente al que atendera
     * @param initialTime Tiempo en el que comienza a atender
     */
    public Trabajador(String nombre, Cliente cliente, long initialTime) {
        this.nombre = nombre;
        this.cliente = cliente;
        this.initialTime = initialTime;
    }

    // Getters y Setters
    public String getNombre() {
        return nombre;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public long getInitialTime() {
        return initialTime;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public void setInitialTime(long initialTime) {
        this.initialTime = initialTime;
    }

    /**
     * Sobrecarga del metodo run que se hereda de Thread, en este metodo
     * se define que es el algoritmo a realizar
     */
    //@Override
    public void run() {

            // Se imprime el comienzo
            System.out.println("El trabajador: " + this.nombre + " COMIENZA A SERVIR LA COMIDA DEL CLIENTE " 
                                    + this.cliente.getNombre() + " EN EL TIEMPO: " 
                                    + (System.currentTimeMillis() - this.initialTime) / 1000 
                                    + "seg");

            // Por cada platillo que solicito el cliente se simula que es atendido
            for (int i = 0; i < this.cliente.getListaPlatillos().length; i++) { 
                    this.esperarXsegundos(cliente.getListaPlatillos()[i]); 
                    System.out.println("Procesado el platillo " + (i + 1) 
                    + " del cliente " + this.cliente.getNombre() + "->Tiempo: " 
                    + (System.currentTimeMillis() - this.initialTime) / 1000 
                    + "seg");
            }

            // Se imprime el resultado de la simulacion
            System.out.println("El trabajador " + this.nombre + " HA TERMINADO DE PROCESAR " 
                                            + this.cliente.getNombre() + " EN EL TIEMPO: " 
                                            + (System.currentTimeMillis() - this.initialTime) / 1000 
                                            + "seg");
    }

    /**
     * Duerme al hilo principal slo para que sea mas entendible la simuulacion
     * @param segundos Tiempo que el hilo permanecera dormido
     */
    private void esperarXsegundos(int segundos) {
        try {
                Thread.sleep(segundos * 1000);
        } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
        }
    }
}
