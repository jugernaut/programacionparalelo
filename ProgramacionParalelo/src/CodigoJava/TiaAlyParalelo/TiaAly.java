package hilos.tiaAly.concurrente;
import hilos.tiaAly.secuencial.Cliente;

/**
 * Clase principla con el metodo main que muestra una diminuta simulacion
 * haciendo uso de hilos en la que clientes son atendidos por trabajadores
 * @author mike
 */
public class TiaAly {

    /**
     * Metodo principal en el que interactuan los clientes y los trabajadores
     * @param args the command line arguments
     */
    public static void main(String[] args) {

                // Se crean 2 clientes con sus respectivos platillos
		Cliente cliente1 = new Cliente("Cliente 1", new int[] { 2, 2, 1, 5, 2, 3 });
		Cliente cliente2 = new Cliente("Cliente 2", new int[] { 1, 3, 5, 1, 1 });
                Cliente cliente3 = new Cliente("Cliente 3", new int[] { 1, 2, 3, 4, 5, 6 });
		Cliente cliente4 = new Cliente("Cliente 4", new int[] { 6, 5, 4, 3, 2, 1 });
                Cliente cliente5 = new Cliente("Cliente 5", new int[] { 1, 2, 1, 2, 1 });

		// Tiempo inicial de referencia
		long initialTime = System.currentTimeMillis();
                // Se genera un par de trabjadores que atenderan a cada uno de los clientes
		Trabajador trabajador1 = new Trabajador("Trabajador 1", cliente1, initialTime);
		Trabajador trabajador2 = new Trabajador("Trabajador 2", cliente2, initialTime);
                Trabajador trabajador3 = new Trabajador("Trabajador 3", cliente3, initialTime);
		Trabajador trabajador4 = new Trabajador("Trabajador 4", cliente4, initialTime);
                Trabajador trabajador5 = new Trabajador("Trabajador 5", cliente5, initialTime);
		
                // Se agregan los hilos que ejecutan a los trabajadores
                Thread thread1 = new Thread(trabajador1);
                Thread thread2 = new Thread(trabajador2);
                Thread thread3 = new Thread(trabajador3);
                Thread thread4 = new Thread(trabajador4);
                Thread thread5 = new Thread(trabajador5);
                
                // Se ejecutan todos los hilos "al mismo tiempo"
                thread1.start();
                thread2.start();
                thread3.start();
                thread4.start();
                thread5.start();
    }
}
