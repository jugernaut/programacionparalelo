package hilos.tiaAly.secuencial;

/**
 * Clase Cliente que tiene como proposito generar varios objetos de tipo
 * Cliente para que puedan ser atendidos por trabajadores sin importar
 * si se realiza de manera concurrente o secuencial
 * @author mike
 */
public class Cliente {
    private String nombre;
    private int[] listaplatillos;

    public Cliente(String nombre, int[] carrocompra) {
        this.nombre = nombre;
        this.listaplatillos = carrocompra;
    }

    public String getNombre() {
        return nombre;
    }

    public int[] getListaPlatillos() {
        return listaplatillos;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setListaPlatillos(int[] listaplatillos) {
        this.listaplatillos = listaplatillos;
    }
}
