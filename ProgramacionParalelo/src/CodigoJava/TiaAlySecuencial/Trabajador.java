package hilos.tiaAly.secuencial;

/**
 * Clase que simula las caracteristicas que nos interesan de un trabjador
 * @author mike
 */
public class Trabajador{

    private String nombre;

    /**
     * Constructor de trabajadores
     * @param nombre Nombre del trabajador
     */
    public Trabajador(String nombre) {
        this.nombre = nombre;
    }

    // Getters y Setters
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    /**
     * Metodo que se encarga de simular el que este Trabajador atienda a un
     * Cliente
     * @param cliente Cliente que sera atendido
     * @param timeStamp Tiempo a partir del cual el Trabajador comienza
     */
    public void atiendePedido(Cliente cliente, long timeStamp) {
                
                // Se imprime el comienzo
		System.out.println("El trabajador: " + this.nombre + 
				" COMIENZA A PROCESAR EL PEDIDO DEL CLIENTE " + cliente.getNombre() + 
				" EN EL TIEMPO: " + (System.currentTimeMillis() - timeStamp) / 1000	+
				"seg");

                // Por cada platillo que solicito el cliente se simula que es atendido
		for (int i = 0; i < cliente.getListaPlatillos().length; i++) { 
				this.esperarXsegundos(cliente.getListaPlatillos()[i]); 
				System.out.println("Procesado el platillo " + (i + 1) +  
				" ->Tiempo: " + (System.currentTimeMillis() - timeStamp) / 1000 + 
				"seg");
		}

                // Se imprime el resultado de la simulacion
		System.out.println("El trabajador " + this.nombre + " HA TERMINADO DE PROCESAR " + 
				cliente.getNombre() + " EN EL TIEMPO: " + 
				(System.currentTimeMillis() - timeStamp) / 1000 + "seg");
    }

    /**
     * Duerme al hilo principal slo para que sea mas entendible la simuulacion
     * @param segundos Tiempo que el hilo permanecera dormido
     */
    private void esperarXsegundos(int segundos) {
        try {
                Thread.sleep(segundos * 1000);
        } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
        }
    }
}
