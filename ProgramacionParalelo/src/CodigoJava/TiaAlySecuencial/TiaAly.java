package hilos.tiaAly.secuencial;

/**
 * Clase principla con el metodo main que muestra una diminuta simulacion
 * secuencial en la que clientes son atendidos por trabajadores
 * @author mike
 */
public class TiaAly {

    /**
     * Metodo principal en el que interactuan los clientes y los trabajadores
     * @param args the command line arguments
     */
    public static void main(String[] args) {

                // Se crean 2 clientes con sus respectivos platillos
		Cliente cliente1 = new Cliente("Cliente 1", new int[] { 2, 2, 1, 5, 2, 3 });
		Cliente cliente2 = new Cliente("Cliente 2", new int[] { 1, 3, 5, 1, 1 });
                Cliente cliente3 = new Cliente("Cliente 3", new int[] { 1, 2, 3, 4, 5, 6 });
		Cliente cliente4 = new Cliente("Cliente 4", new int[] { 6, 5, 4, 3, 2, 1 });
                Cliente cliente5 = new Cliente("Cliente 5", new int[] { 1, 2, 1, 2, 1 });

		// Tiempo inicial de referencia
		long initialTime = System.currentTimeMillis();
                // Se genera un par de trabjadores que atenderan a cada uno de los clientes
		Trabajador trabajador1 = new Trabajador("Trabajador 1");
		Trabajador trabajador2 = new Trabajador("Trabajador 2");
                Trabajador trabajador3 = new Trabajador("Trabajador 3");
		Trabajador trabajador4 = new Trabajador("Trabajador 4");
                Trabajador trabajador5 = new Trabajador("Trabajador 5");
                // Comienza a atender el trabajador1 seguido del trabajador2
		trabajador1.atiendePedido(cliente1, initialTime);
		trabajador2.atiendePedido(cliente2, initialTime);
                trabajador3.atiendePedido(cliente3, initialTime);
		trabajador4.atiendePedido(cliente4, initialTime);
                trabajador5.atiendePedido(cliente5, initialTime);
    }
}
