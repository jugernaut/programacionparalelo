#include <omp.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

//gcc -o aproxpibasico -fopenmp aproxpibasico.c -lm
 
int main(){
	int n;
	double PI, h, sum, error, pi, INICIO, FIN, tiempof;
	printf("introduce la precision del calculo (n > 0): ");
	scanf("%d", &n);
	pi = 0.0;	
	PI = 3.141592653589793238462643;
	h = 1.0 / (double) n;
	sum = 0.0;
	error = 0.0;
	for (int i = 1; i <= n; i++) {
		double x = h * ((double)i);
		sum += (sqrt(1.0 - x*x));
	}
	pi = (sum * 4)*h;
	printf("El valor aproximado de pi es: %f\n", pi);
}
