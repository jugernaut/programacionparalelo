'''
Created on 27 abr. 2020
@author: mike
'''
# importamos la libreria
import tensorflow as tf

#Se define el grafo
grafo1 = tf.Graph()

#Se define la sesion pasandole el grafo
with tf.Session(graph=grafo1) as sess:
    #Se define las constantes x y y que son arreglos
    x = tf.constant([11,13,16])
    y = tf.constant([10,10,10])
    #Se realiza la operacion de suma de x+y
    op = tf.add(x,y)
    #Se ejecuta la operacion en la sesion
    resultado = sess.run(op)
    print("Suma de vectores: \n{}".format(resultado))
    
#Se define el grafo
grafo2 = tf.Graph()


with tf.Session(graph=grafo2) as sess: 
    # creamos constantes a=2 y b=3
    a = tf.constant(2)
    b = tf.constant(3)
    
    # creamos matrices de 3x3
    matriz1 = tf.constant([[1, 3, 2],
                           [1, 0, 0],
                           [1, 2, 2]])
    
    matriz2 = tf.constant([[1, 0, 5],
                           [7, 5, 0],
                           [2, 1, 1]])
    
    # Realizamos algunos calculos con estas constantes
    suma = tf.add(a, b)
    mult = tf.multiply(a, b)
    cubo_a = a**3
    
    # suma de matrices
    suma_mat = tf.add(matriz1, matriz2)
    
    # producto de matrices
    mult_mat = tf.matmul(matriz1, matriz2)
    
    print("Suma de las constantes: {}".format(sess.run(suma)))
    print("Multiplicacion de las constantes: {}".format(sess.run(mult)))
    print("Constante elevada al cubo: {}".format(sess.run(cubo_a)))
    print("Suma de matrices: \n{}".format(sess.run(suma_mat)))
    print("Producto de matrices: \n{}".format(sess.run(mult_mat)))
