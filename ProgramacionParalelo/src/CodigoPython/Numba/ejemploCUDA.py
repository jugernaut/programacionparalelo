'''
Created on 22 abr. 2020
@author: mike
'''
# para poder usar CUDA
from numba import cuda
import numpy as np
import math
import time
# identificamos si se cuenta con CUDA
print(cuda.gpus)

@cuda.jit
def incremento_en_uno_paralelo(arreglo):
    # Identificador del hio en un bloque de una dimension
    tx = cuda.threadIdx.x
    # Identificador del bloque en un grid de una dimension
    ty = cuda.blockIdx.x
    # Ancho del bloque, i.e. numero de hilos por bloque
    bt = cuda.blockDim.x
    # Calculo del indicice "aplanado" en un arreglo (polinomio de direccionamiento)
    pos = tx + ty * bt
    if pos < arreglo.size:  # revisamos los limites del arreglo
        arreglo[pos] += 1   
        
def incremento_en_uno(arreglo):
    for pos in range(len(arreglo)):
        arreglo[pos] += 1 
    return arreglo   
        
def main():
    # datos Numba GPU
    datosp = np.ones(256)
    hilosporbloque = 32
    bloquesporgrid = (datosp.shape[0] + hilosporbloque - 1) // hilosporbloque
    
    # datos secuencial
    datos = np.ones(256)
    
    inicio = time.time()
    incremento_en_uno(datos)
    fin = time.time()
    print("Transcurrio (secuencial) = %s" % (fin - inicio))
    print(datos)
    
    inicio = time.time()
    incremento_en_uno_paralelo[bloquesporgrid, hilosporbloque](datosp)
    fin = time.time()
    print("Transcurrio (en paralelo) = %s" % (fin - inicio))
    print(datosp)
    
main()
