'''
Created on 27 abr. 2020
@author: mike
'''
from numba import jit, prange, njit
import numpy as np
import time

x = np.arange(100).reshape(10, 10)

@jit(nopython=True)
def ir_mas_rapido(a): # Funcion que es compilada y ejecutada en codigo de maquina
    traza = 0
    for i in range(a.shape[0]):
        traza += np.tanh(a[i, i])
    return a + traza

@njit(parallel=True)
def integralMonteCarloGP(N, a, b):
    puntos = np.random.uniform(a, b, size=N)
    total=0
    for i in prange(N):
        total += puntos[i]*puntos[i]
    aprox = (b-a)*total/N
    return aprox

def integralMonteCarloG(f, N, a, b):
    puntos = np.random.uniform(a, b, size=N)
    total=0
    for i in range(N):
        total+=f(puntos[i])
    aprox = (b-a)*total/N
    return aprox

def f(x):
    return x**2

def main():
    '''
    # Unicamente se toma el tiempo de ejecucion de la funcion!
    inicio = time.time()
    ir_mas_rapido(x)
    fin = time.time()
    print("Transcurrio (con compilacion) = %s" % (fin - inicio))
    
    # UNA VEZ QUE LA FUNCION ESTA ALMACENADA EN CACHE SE VUELVE A MEDIR EL TIEMPO
    inicio = time.time()
    ir_mas_rapido(x)
    fin = time.time()
    print("Transcurrio (sin compilacion) = %s" % (fin - inicio))'''
    
    inicio = time.time()
    print integralMonteCarloG(f, 1000000, 1.0, 2.0)
    fin = time.time()
    print("Transcurrio (sin paralelizar) = %s" % (fin - inicio))
    
    inicio = time.time()
    print integralMonteCarloGP(1000000, 1.0, 2.0)
    fin = time.time()
    print("Transcurrio (en paralelo) = %s" % (fin - inicio))
    
main()