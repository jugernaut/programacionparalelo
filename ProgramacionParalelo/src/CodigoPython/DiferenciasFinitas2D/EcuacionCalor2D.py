#!/usr/bin/env python

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import math
import os
fig = plt.figure()
#Ruta para guardar los resultados
path = './resultados/'
if not os.path.isdir(path): os.makedirs(path)

def dif_fin_exp(K,t1,t2,t3,t4,Nx,Ny,Nt,xmax,xmin,ymax,ymin,tmax,malla):
    """Metodo de Diferencias Finitas usando un esquema Explicito
    Encuentra el valor de la temperatura en un punto (x,y) dentro 
    de una malla en un tiempo dado. Para encontrar dicho valor se
    usa el esquema explicito.
    Argumentos
    ----------
    K - coeficiente de conductividad termica
    t1 - condicion de frontera 
    t2 - condicion de frontera
    t3 - condicion de frontera
    t4 - condicion de frontera
    Nx - numero de puntos en x
    Ny - numero de puntos en
    Nt - pasos de tiempo (valor necesario para calcular sx y sx)
    xmax - valor maximo de los puntos en x
    xmin - valor minimo de los puntos en x
    ymax - valor maximo de los puntos en y
    xmin - valor minimo de los puntos en y
    tmax - valor maximo para el tiempo (el valor minimo es cero por la condicion inical del problema)
    malla - arreglo bidimensional que representa la malla en el tiempo n-1
    Devuelve
    --------
    mallaActual - arrgelo bidimensional que representa la malla en el tiempo n 
    -----------
    RuntimeError - No hubo convergencia debido a la inestabilidad del algoritmo
    """
    #caluclo para delta x, y, t
    dx=(xmax-xmin)/Nx
    dy=(ymax-ymin)/Ny
    dt=(tmax)/Nt 
    #calculo de sx,sy y D como se muestra en la presentacion de diferencias finitas 2   
    sx=K*dt/(dx**2)
    sy=K*dt/(dy**2)
    D=sx+sy
    #malla que almacenara la temperatura en el punto (x,y) al paso de tiempo n
    mallaActual = np.zeros((Ny-1,Nx-1))
    #condicion de estabilidad
    if D>=0.5:
        raise RuntimeError("El algoritmo no converge debido a que es inestable ")
    #par de ciclos anidados que recorren la malla para el calculo de la temperatura    
    for i in xrange(Ny-1):
        for j in xrange(Nx-1):
            #valor en el punto a evaluar
            cen=malla[i][j]
            """si el punto a evaluar colinda con la frontera, se toma el valor de la
                frontera para realizar el calculo de la temperatura en ese punto"""
            aba=t1 if i==0 else  malla[i-1][j] 
            izq=t2 if j==0 else malla[i][j-1] 
            arr=t3 if i==Ny-2 else  malla[i+1][j]          
            der=t4 if j==Nx-2 else malla[i][j+1]
            #calculo del valor de x1 y y1  de acuerdo con el metodo de diferencias finitas          
            x1=(izq-2.0*cen+der)*sx            
            y1=(aba-2.0*cen+arr)*sy
            #se almacena el valor de la temperatura anterior mas el recien calculado
            mallaActual[i][j]=malla[i][j]+x1+y1
    return mallaActual

def graf_malla(T,Nx,Ny,xg,yg,nombre): 
    """Metodo para graficar la malla generada para este problema
    Dada el arreglo (T), se grafica el valor de la temperatura en
    el punto (x,y), de la malla recibida como parametro. Se utiliza
    un graditente de color para determinar la temperatura mas elevada.
    Argumentos
    ----------
    T - arreglo con los valores de las temperaturas para el punto (x,y) 
    Nx - numero de puntos en x 
    Ny - numero de puntos en y
    xg - arreglo con los valores de las coordenadas en el eje x
    xy - arreglo con los valores de las coordenadas en el eje y
    nombre - nombre de la grafica que sera generada por el metodo
    """   
    zs=[]
    #se llena el arreglo para los valores de z
    for i in xrange(Ny-1):
        for j in xrange(Nx-1):
            zs.append(T[i][j])
            
    plt.clf()
    ax = fig.gca(projection='3d')
    #grafica usando un gradiente de color
    ax.scatter(xg,yg, zs, s=30, c=zs, marker='o')
    ax.view_init(elev=10., azim=80)
    ax.set_xlabel('X ')
    ax.set_ylabel('Y ')
    ax.set_zlabel('Z ')
    #se guarda la imagen para poder generar una animacion
    filename = os.path.join(path, str(nombre))
    plt.savefig(filename)
    
def graficar_sol(T,Nx,Ny,xg,yg,nombre):
    zs=[]
    #se llena el arreglo para los valores de z
    for i in xrange(Nx-1):
        for j in xrange(Ny-1):
            zs.append(T[i][j])
            
    plt.clf()
    ax = fig.gca(projection='3d')
    #grafica usando un gradiente de color
    ax.scatter(xg,yg, zs, s=30, c=zs, marker='o')
    ax.view_init(elev=10., azim=80)
    ax.set_xlabel('X ')
    ax.set_ylabel('Y ')
    ax.set_zlabel('Z ')
    #se guarda la imagen para poder generar una animacion
    filename = os.path.join(path, str(nombre))
    plt.savefig(filename)
    
def solucion_exacta(x,y):
    """Genera la soucion analitica para la temperatura en el punto x,y"""
    t = 0
    for n in xrange(1,100):
        t += ((1-(-1**n))/(n*math.pi))*(math.sinh(n*math.pi*(2-y))/math.sinh(n*math.pi*2))*math.sin(n*math.pi*x)
    t = 100*(2*t)
    return t  

def genera_solucion_exacta(Nx, Ny, x,y):
    """
    Se crea una matriz de (Ny-1,Nx-1) para almacenar las temperaturas exactas
    de una placa en la cual se distribuye el calor
    """
    sol_exacta = np.zeros((Ny-1,Nx-1))
    col=0;
    reng = 0;             
    for i in xrange(Ny-1):
        for j in xrange(Nx-1):
            sol_exacta[i][j] = solucion_exacta(x[reng], y[col])   
            col+=1
        col = 0    
    reng+=1
    return sol_exacta

def main():
    """Funcion principal que se encarga de llamar a los metodos definidos
    en este modulo para poder reslver el problema de la difusion termica"""
    #Condiciones
    a=0.0       #condicion inicial
    b=100.0     #cond front aba
    c=0.0       #cond front izq
    d=0.0       #cond front arr    
    e=0.0       #cond front der
    #Intervalos y pasos de tiempo
    Nx=20       #puntos en x 
    Ny=2*Nx     #puntos en y
    tmax=100.0  #tiempo maximo
    Nt=int(4*tmax*Nx**2 +1) #pasos de tiempo
    xmax=1.0    #maximo valor en x   
    xmin=0.0    #minimo valor en x
    ymax=2.0    #maximo valor en y
    ymin=0.0    #minimo valor en y
    #Deltas
    dx=(xmax-xmin)/Nx #delta x
    dy=(ymax-ymin)/Ny #delta y
    #Coeficiente de conductividad termica
    K = 1
    
    #se genera una malla inicial llena con el valor de la condicion inicial
    #y un malla similar auxiliar llena de ceros
    malla_anterior=np.tile(a, (Ny-1,Nx-1))
    malla_actual=np.zeros((Ny-1,Nx-1))
    
    #arrelo de puntos que determina el espacio de la malla
    x = np.linspace(xmin+dx,xmax-dx,Nx-1)
    y = np.linspace(ymin+dy,ymax-dy,Ny-1)
    #se genera el grid necesario para graficar
    xg, yg = np.meshgrid(x,y)

    #Se genera la grafica para el tiempo inicial
    con=0
    graf_malla(malla_anterior,Nx,Ny,xg,yg,"Tiempo_Inicial")
    
    #Para cada paso de tiempo se genera una nueva grafica
    for i in xrange(1,Nt):
        #La malla en el tiempo n-1, sirve de parametro para generar la nueva malla
        malla_actual = dif_fin_exp(K,b,c,d,e,Nx,Ny,Nt,xmax,xmin,ymax,ymin,tmax,malla_anterior)
        malla_anterior = malla_actual
        if i>=1 and i<=100 and con<100:
            #Se grafica y se guarda la imagen generada
            graf_malla(malla_actual,Nx,Ny,xg,yg,str("Tiempo")+str(i))
            con+=1
        if con>=100:
            break
        
    #Se calcula la solucion analitica para realizar las comparaciones   
    sol_exacta = genera_solucion_exacta(Nx, Ny, x, y)     
    
    sol_analitica = [list(i) for i in zip(*sol_exacta)]
    graficar_sol(sol_analitica,Nx,Ny,xg,yg, "Solucion_Analitica")  
    
    #Error que sirve para generar grafica log(e) v.s. log(h)
    error3 = np.linalg.norm(sol_exacta, -2) - np.linalg.norm(malla_actual, -2)
    
    print "+----------------------------------------------------+"
    print "|      Solucion a la ecuacion del calor en 2D        |"
    print "|              con diferencias finitas:              |"
    print "|                 Esquema explicito.                 |"
    print "+----------------------------------------------------+"
    print "| Autor:     Perez Leon Miguel Angel                 |"
    print "+----------------------------------------------------+"
    print "|                 Datos de entrada                   |"
    print "+----------------------------------------------------+"
    print "| Punto inicial del dominio en x   : ax = %g" % xmin
    print "| Punto final del dominio en x     : bx = %g" % xmax
    print "| Punto inicial del dominio en y   : ax = %g" % ymin
    print "| Punto final del dominio en y     : bx = %g" % ymax
    print "| Numero total de incognitas en x  : Nx = %d" % Nx
    print "| Numero total de incognitas en y  : Ny = %d" % Ny
    print "| Numero total de incognitas       : N = %d" % (Nx*Ny)
    print "| La conduccion termica es       : K = %g  " % K
    print "| Cond. de front. en T1 : b= %g" % b
    print "| Cond. de front. en T2 : c= %g" % c
    print "| Cond. de front. en T3 : d= %g" % d
    print "| Cond. de front. en T4 : e= %g" % e
    print "+----------------------------------------------------+"
    
    return 0

if __name__ == '__main__':
    main()
