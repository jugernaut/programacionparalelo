from SOM import SOM
import numpy as np
from matplotlib import pyplot as plt
 
# Vectores de entrenamiento RGBcolors
colors = np.array(
     [[0., 0., 0.],
      [0., 0., 1.],
      [0., 0., 0.5],
      [0.125, 0.529, 1.0],
      [0.33, 0.4, 0.67],
      [0.6, 0.5, 1.0],
      [0., 1., 0.],
      [1., 0., 0.],
      [0., 1., 1.],
      [1., 0., 1.],
      [1., 1., 0.],
      [1., 1., 1.],
      [.33, .33, .33],
      [.5, .5, .5],
      [.66, .66, .66]])
color_names = \
    ['negro', 'azul', 'azul marino', 'azul cielo',
     'gris azulado', 'lila', 'verde', 'rojo',
     'cyan', 'violeta', 'amarillo', 'blanco',
     'gris obscuro', 'gris medio', 'gris claro']
 
# Creamos un SOM de 20x30 y se entrena 00 veces
som = SOM(20, 30, 3, 400)

# Se muestra el mapa inicial
mapa_inicial = som._mapa_inicial
plt.imshow(mapa_inicial)
plt.title('Red Neuronal Inicial')
plt.show()

# Se entrena la red con un conjunto de colores
som.train(colors)
 
# Obtenemos el SOM ya entrenado
image_grid = som.get_centroids()
 
# Contiene la lista de coordenadas de los correspondites colores
mapped = som.map_vects(colors)
 
# Grafica
plt.imshow(image_grid)
plt.title('Red Neuronal Entrenada')
for i, m in enumerate(mapped):
    plt.text(m[1], m[0], color_names[i], ha='center', va='center',
             bbox=dict(facecolor='white', alpha=0.5, lw=0))
plt.show()

# Se le muestra un color para que indique a que neurona se parece mas
print som.map_vect([0.125, 0.529, 1.0])


