import random
from time import time

#Ordenamiento rapido O(n*log n)
def quickSort(lista):
    if len(lista) < 2:
        return lista
    else:
        menores, pivote, mayores = particion(lista)
        return quickSort(menores)+[pivote]+quickSort(mayores)

#funcion auxiliar que parte una lista en mayores, menores y pivote O(n)
def particion(lista):
    mayores, menores = [], []
    pivote = lista[0]
    for i in range(1,len(lista)):
        if lista[i] < pivote:
            menores.append(lista[i])
        else:
            mayores.append(lista[i])
    return menores, pivote, mayores

def main():
    lista100I = random.sample(range(1000),100)
    tiempoi = time()
    quickSort(lista100I)
    tiempof = time()
    print tiempof-tiempoi
    
main()



