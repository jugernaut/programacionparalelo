'''
Created on 20/03/2020

@author: mike
'''
import matplotlib.pyplot as plt
import numpy as np
import math
import os
fig = plt.figure()

# Ruta para guardar los resultados
ruta = './resultados/'
# En caso de no existir el directorio, se crea el directorio
if not os.path.isdir(ruta): os.makedirs(ruta)

def diferenciasFinitasEC(K, a, b, c, Nx, dt, Nt, xmax):
    """Metodo de Diferencias Finitas usando un esquema Explicito
    Encuentra el valor de la temperatura y en un punto x dentro 
    de una barra de un material K, dado un tiempo t.
    Para encontrar dicho valor se usa el esquema explicito.
    Argumentos
    ----------
    K - coeficiente de conductividad termica
    a - condicion inicial (temperatura inicial) en T0condicion de frontera (temperatura en uno de los extremos)
    b - condicion de frontera (temperatura en uno de los extremos)
    c - condicion de frontera (temperatura en otro de los extremos)
    Nx - numero de puntos en x
    dt - distancia entre cada paso de tiempo
    Nt - pasos de tiempo
    xmax - valor maximo de los puntos en x
    Devuelve
    -----------
    Matriz T de dimension (Nx,Nt) que contiene los valores de la temperatura
    en cada punto de la barra por cada paso de tiempo. En cada renglon se 
    guarda la temperatura de un valor x para todos los pasos de tiempo.
    En otras palabras cada columna representa la temperatura de toda la barra
    en un paso de tiempo dado t.
    -----------
    RuntimeError - No hubo convergencia debido a la inestabilidad del algoritmo
    """
    # matriz de dimension (Nx,Nt) que almacena las tempraturas, inicialmete 0
    T = np.zeros((Nx, Nt))
    # delta x, distancia entre cada punto de la barra
    dx = (float(xmax)/float(Nx))
    # condicion de estabilidad
    D = K*dt/(dx*dx)
    if D>=0.5:
        raise RuntimeError("El algoritmo no converge debido a que es inestable")
    # condicion de frontera en el extremo izquierdo de la barra
    T[0] = b
    T[Nx-1] = c
    # concion inicial (temperatura en toda la barra en el tiempo cero)
    for i in range(0, Nx):
        T[i][0]= a
    # ciclo principal que recorre las columnas de la matriz T
    for n in range(0,Nt-1):
        # ciclo que recorre los renglores de la matriz T
        for i in range(1,Nx-1):
            # ecuacion que se muestra en la presentacion Modelos Matematicos
            T[i][n+1]=T[i][n]+D*(T[i+1][n]-2*T[i][n]+T[i-1][n]) 
    return T

def diferenciasFinitasEC2(K, a, b, c, Nx, dt, Nt, xmax):
    """Metodo de Diferencias Finitas usando un esquema Explicito
    A diferencia del metodo diferenciasFinitasEC, en este se almacenan
    los pasos de tiempo en los renglones y las temperaturas en las columnas."""
    T = np.zeros((Nt, Nx))
    dx = (float(xmax)/float(Nx))
    D = K*dt/(dx*dx)
    if D>=0.5:
        raise RuntimeError("El algoritmo no converge debido a que es inestable")
    T[1] = float(a)
    for i in range(1, Nt):
        T[i][0] = b
        T[i][Nx-1] = c
    for n in range(1,Nt-1):
        for i in range(1,Nx-1):
            T[n+1][i]=T[n][i]+D*(T[n][i+1]-2*T[n][i]+T[n][i-1]) 
    return T

def diferenciasFinitasECImpicito(K, a, b, c, Nx, dt, Nt, xmax):
    """Metodo de Diferencias Finitas usando un esquema Explicito
    Encuentra el valor de la temperatura y en un punto x dentro 
    de una barra de un material K, dado un tiempo t.
    Para encontrar dicho valor se usa el esquema explicito.
    Argumentos
    ----------
    K - coeficiente de conductividad termica
    a - condicion inicial (temperatura inicial) en T0condicion de frontera (temperatura en uno de los extremos)
    b - condicion de frontera (temperatura en uno de los extremos)
    c - condicion de frontera (temperatura en otro de los extremos)
    Nx - numero de puntos en x
    dt - distancia entre cada paso de tiempo
    Nt - pasos de tiempo
    xmax - valor maximo de los puntos en x
    Devuelve
    -----------
    Matriz T de dimension (Nx,Nt) que contiene los valores de la temperatura
    en cada punto de la barra por cada paso de tiempo. En cada renglon se 
    guarda la temperatura de un valor x para todos los pasos de tiempo.
    En otras palabras cada columna representa la temperatura de toda la barra
    en un paso de tiempo dado t.
    -----------
    RuntimeError - No hubo convergencia debido a la inestabilidad del algoritmo
    """
    # matriz de dimension (Nt,Nx) que almacena las tempraturas, inicialmete 0
    T = np.zeros((Nt, Nx))
    # delta x, distancia entre cada punto de la barra
    dx = (float(xmax)/Nx)
    # matriz necesaria para realizar esquema implicito (revisar presentacion)
    mat = np.zeros((Nx, Nx))
    # factor que multiplica entradas de la matriz (revisar presentacion)
    r = ((dx**2)/(K*dt))
    # se llena la diagonal y los elementos a la izquierda y derecha de la diag.
    for i in range(1,len(mat)-1):
        for j in range(len(mat)):
            # solo se modifican los elementos de la diagonal y los de los lados
            if i == j:
                mat[i][i] = r+2
                mat[i][i-1] = -1
                mat[i][i+1] = -1 
    # elementos en los extremos de las esquinas de la matriz (revisar present.)
    mat[0][0] = r+2
    mat[0][1] = -1
    mat[Nx-1][Nx-1] = r+2
    mat[Nx-1][Nx-2] = -1
    # condiciones INICIALES y de FRONTERA
    T[1] = float(a)
    T[1][0] = float(b)
    T[1][Nx-1] = float(c)
    """
    Por cada paso de tiempo se resuelve un sistema de ecuaciones del tipo
    Ax=b en el cual en cada paso de tiempo se usa la solicion anterior para
    encontrar la nueva solucion del sistema. Cada renglon de la matriz T
    representa una solución para cada paso de tiempo. Comienza desde tiempo=2
    ya que en el tiempo T[0] la temperatura es cero y en T[1] se tienen las
    condiciones iniciales y de frontera
    """
    for tiempo in range(2,len(T)):
        # se copia la solucion anterior ya que se usa para encontrar la nueva sol
        nuevasol = T[tiempo-1].copy()
        # la solucion anterior se multiplica por r para encontrar la nueva sol
        # revisar presentacion de Modelos
        for i in range(len(nuevasol)):
            nuevasol[i] = nuevasol[i]*r
        # se actualizan los valores de la frontera de la nueva solucion
        # revisar presentacion de Modelos
        nuevasol[0] = nuevasol[0]+b
        nuevasol[len(nuevasol)-1] = nuevasol[0]+c
        # se resuelve el nuevo sista Ax=nuevasol
        temperatura = np.linalg.solve(mat, nuevasol)
        # se almacena la temperatura en T
        T[tiempo] = temperatura
    return T

def graficar_sol(T,Nx,xmax):
    """Metodo que grafica las temperaturas de una barra por renglones
    ----------
    T - matriz de (Nt,Nx) que contiene temperaturas de una barra
    a - numero de puntos sobre la barra
    xmax - maximo valor sobre x
    Devuelve
    -----------
    Almacena en /resultados las graficas de las tempertaturas de T
    """
    xg = np.linspace(0, xmax, Nx)
    t = 0     
    for tiempo in T:
        plt.clf()
        color = [str(item/255.) for item in tiempo]
        plt.scatter(xg, tiempo, s=100, c=color) 
        plt.plot(xg, tiempo, '.r-')
        plt.title('Diferencias Finitas Ecuacion Calor')
        #se guarda la imagen para poder generar una animacion
        nombre = 'tiempo'+str(t)
        filename = os.path.join(ruta, str(nombre))
        plt.savefig(filename)
        t+=1
        
def graficar_sol2(T,Nx,xmax):
    """Metodo que grafica las temperaturas de una barra por columnas
    ----------
    T - matriz de (Nx,Nt) que contiene temperaturas de una barra
    a - numero de puntos sobre la barra
    xmax - maximo valor sobre x
    Devuelve
    -----------
    Almacena en /resultados las graficas de las tempertaturas de T
    """
    # puntos sobre x
    xg = np.linspace(0, xmax, Nx)
    # variable para el nombre de las graficas
    t = 0     
    # por cada tiempo se genera una grafica
    for tiempo in range(len(T[0])):
        ti = T[:,tiempo]
        plt.clf()
        color = [str(item/255.) for item in ti]
        plt.scatter(xg, ti, s=100, c=color) 
        plt.plot(xg, ti, '.r-')
        plt.title('Diferencias Finitas Ecuacion Calor')
        
        #se guarda la imagen para poder generar una animacion
        nombre = 'tiempo'+str(t)
        filename = os.path.join(ruta, str(nombre))
        plt.savefig(filename)
        t+=1

# Pruebas de los diferentes metodos
#graficar_sol(diferenciasFinitasEC2(math.e**-2, 323, 273, 373, 25, 0.001, 100, 0.5), 25, 0.5)
#graficar_sol2(diferenciasFinitasEC(math.e**-2, 323, 273, 373, 25, 0.001, 100, 0.5), 25, 0.5)
graficar_sol(diferenciasFinitasECImpicito(math.e**-2, 323, 273, 373, 25, 0.001, 100, 0.5), 25, 0.5)

